(define (script-fu-batch-save-as-png globexp outpath)
  (define (save-img n f)
    (let* ((fname (car f))
          (img (car (gimp-file-load 1 fname fname)))	  
		  
		  (filename (substring (car (gimp-image-get-name img)) 0 (- (string-length (car (gimp-image-get-name img))) 4)))
		  
		  (pngname (string-append filename ".png"))
		  		  
		  (outname (string-append outpath pngname))
		  
		  )
		 
		(gimp-image-undo-disable img)
		(gimp-file-save 1 img (car (gimp-image-get-active-drawable img)) outname outname)
		(gimp-image-delete img)
    )
    (if (= n 1) 1 (save-img (- n 1) (cdr f)))
  )
  
  (let* ((files (file-glob globexp 0)))  
	(save-img (car files) (car (cdr files))))
)

(script-fu-register "script-fu-batch-save-as-png"
		    "<Image>/Script-Fu/Batch/Save as png ..."
		    "Multiple images save as png"
		    "axoona (@axoonaa)"
		    "Copyright 2022, axoona"
		    "November 8, 2022"
		    ""
		    SF-STRING "Input files" "IN_PATH\\*.jpg"			
			SF-STRING "Out path" "OUT_PATH\\")