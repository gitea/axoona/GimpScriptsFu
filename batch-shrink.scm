(define (script-fu-batch-shrink globexp outpath fctr)
  (define (resize-img n f)
    (let* (
		  (fname (car f))
          (img (car (gimp-file-load 1 fname fname)))
		  (newy (/ (car (gimp-image-height img)) fctr))
		  (newx (/ (car (gimp-image-width img)) fctr))
		  (outname (string-append outpath (car (gimp-image-get-name img)))))
		
		(gimp-image-undo-disable img)
	  
			
			
      (gimp-image-scale img newx newy)
      (gimp-file-save 1 img (car (gimp-image-get-active-drawable img)) outname outname)
      (gimp-image-delete img)
    )
    (if (= n 1) 1 (resize-img (- n 1) (cdr f)))
	)
  
    (let* ((files (file-glob globexp 0)))  
  (resize-img (car files) (car (cdr files))))
)

(script-fu-register "script-fu-batch-shrink"
		    "<Image>/Script-Fu/Batch/Shrink ..."
		    "Multiple JPG shrink batch process"
		    "axoona (@axoonaa)"
		    "Copyright 2015, axoona"
		    "Nov 17, 2015"
		    ""
		    SF-STRING "In path" "IN_PATH\\*"
			SF-STRING "Out path" "OUT_PATH\\"
		    SF-VALUE "Shrink factor" "4")
