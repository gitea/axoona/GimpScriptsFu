(define (script-fu-add-vguides img layer dist)
  (define (add-vguide y)
	(gimp-image-add-vguide img y)
	(if (= y 0) 1 (add-vguide (- y dist)))
  )
  
  ;(let ((N (/ (car (gimp-image-width img)) dist)))
	(add-vguide (car (gimp-image-width img)))
	;(add-vguide 0)
;)
  
)


(script-fu-register "script-fu-add-vguides"
		    "<Image>/Script-Fu/Guides/Vertical ..."
		    "add vertical guides every X interval"
		    "axoona (@axoonaa)"
		    "Copyright 2016, axoona"
		    "Dec 10, 2015"
		    ""
			SF-IMAGE    "Image"         0
            SF-DRAWABLE "Layer - non used" 0
            SF-VALUE    "Interval" "200"
)
		    