(define (script-fu-add-hguides img layer dist)
  (define (add-hguide y)
	(gimp-image-add-hguide img y)
	(if (= y 0) 1 (add-hguide (- y dist)))
  )
  
  ;(let ((N (/ (car (gimp-image-width img)) dist)))
	(add-hguide (car (gimp-image-height img)))
	;(add-vguide 0)
;)
  
)


(script-fu-register "script-fu-add-hguides"
		    "<Image>/Script-Fu/Guides/Horizontal ..."
		    "add horizontal guides every X interval"
		    "axoona (@axoonaa)"
		    "Copyright 2016, axoona"
		    "Sept 03, 2016"
		    ""
			SF-IMAGE    "Image"         0
            SF-DRAWABLE "Layer - non used" 0
            SF-VALUE    "Interval" "200"
)
		    