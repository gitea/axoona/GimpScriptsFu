(define (script-fu-batch-enlarge globexp outpath ratio inpol)
  (define (resize-img n f)
    (let* ((fname (car f))
          (img (car (gimp-file-load 1 fname fname)))
		  (outname (string-append outpath (car (gimp-image-get-name img))))
		  
		(newy (* (car (gimp-image-height img)) ratio))
		(newx (* (car (gimp-image-width img)) ratio)))
		
		(gimp-image-undo-disable img)
		(gimp-image-scale-full img newx newy inpol)
		(gimp-file-save 1 img (car (gimp-image-get-active-drawable img)) outname outname)
		(gimp-image-delete img)
    )
    (if (= n 1) 1 (resize-img (- n 1) (cdr f)))
  )
  
  ;(set! files (file-glob globexp 0))
  (let* ((files (file-glob globexp 0)))  
  (resize-img (car files) (car (cdr files))))
)

(script-fu-register "script-fu-batch-enlarge"
		    "<Image>/Script-Fu/Batch/Enlarge by ratio ..."
		    "Multiple images resize batch process"
		    "axoona (@axoonaa)"
		    "Copyright 2016, axoona"
		    "April 16, 2016"
		    ""
		    SF-STRING "Input files" "C:\\Users\\Ania\\Documents\\Gry\\Dev\\Unity\\Fifth Exponent\\original graphics\\png\\*"
			SF-STRING "Out path" "C:\\Users\\Ania\\Documents\\Gry\\Dev\\Unity\\Fifth Exponent\\Assets\\graphics\\gimp\\"
		    SF-VALUE "Ratio" "3.0"
			SF-VALUE "Interpolation none-0, linear-1, cubic-2, lanczos-3" "0")
