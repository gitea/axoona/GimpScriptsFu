# ScriptsFu

mes script-fu pour gimp qui sont carrement fous

---------------------------------------

"add horizontal guides every X interval"

"add vertical guides every Y interval"

"Multiple images resize batch process"

"Multiple JPG shrink batch process"

"Multiple images save as png batch process"

-------------------------------------------

placez les fichiers dans : C:\Users***\AppData\Roaming\GIMP\2.10\scripts

ils vont se charger au lancement de GIMP ou sinon vous pouvez aussi cliquer sur "Filters/Actualiser Script-Fu"

---------------------------------------

aide pour commencer à écrire vos script-fu :
https://docs.gimp.org/en/gimp-using-script-fu-tutorial.html

doocumentation fde reference pour Scheme (language utilisé pour script-fu) :
https://groups.csail.mit.edu/mac/ftpdir/scheme-7.4/doc-html/scheme_toc.html
